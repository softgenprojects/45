export const mapStatusToLabel = (statusCode) => {
  const statusMap = {
    1: 'Pending',
    2: 'In Progress',
    3: 'Completed'
  };
  return statusMap[statusCode] || 'Unknown Status';
};