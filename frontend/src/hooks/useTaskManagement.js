import { useMutation, useQuery, useQueryClient } from 'react-query';
import { createTask, assignTask, updateTask, trackTaskProgress } from '@api/TasksApi';

export const useTaskManagement = () => {
  const queryClient = useQueryClient();

  const mutationCreateTask = useMutation(createTask, {
    onSuccess: () => {
      queryClient.invalidateQueries('tasks');
    }
  });

  const mutationAssignTask = useMutation(assignTask, {
    onSuccess: () => {
      queryClient.invalidateQueries('tasks');
    }
  });

  const mutationUpdateTask = useMutation(updateTask, {
    onSuccess: () => {
      queryClient.invalidateQueries('tasks');
    }
  });

  const queryTrackTaskProgress = useQuery('trackTaskProgress', trackTaskProgress);

  return {
    createTask: mutationCreateTask.mutate,
    assignTask: mutationAssignTask.mutate,
    updateTask: mutationUpdateTask.mutate,
    trackTaskProgress: queryTrackTaskProgress.data
  };
};
