import { useMutation, useQuery, useQueryClient } from 'react-query';
import { getProjects, createProject, updateProject, deleteProject } from '@api/ProjectsApi';

export const useProjectManagement = () => {
  const queryClient = useQueryClient();

  const { data: projects, isLoading, isError } = useQuery('projects', getProjects);

  const createMutation = useMutation(createProject, {
    onSuccess: () => {
      queryClient.invalidateQueries('projects');
    },
  });

  const updateMutation = useMutation(updateProject, {
    onSuccess: () => {
      queryClient.invalidateQueries('projects');
    },
  });

  const deleteMutation = useMutation(deleteProject, {
    onSuccess: () => {
      queryClient.invalidateQueries('projects');
    },
  });

  return {
    projects,
    isLoading,
    isError,
    createProject: createMutation.mutate,
    updateProject: updateMutation.mutate,
    deleteProject: deleteMutation.mutate,
  };
};