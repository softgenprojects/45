import React from 'react';
import { Button, FormControl, FormLabel, Input, VStack } from '@chakra-ui/react';
import { useProjectManagement } from '@hooks/useProjectManagement';

export const ProjectForm = ({ onSubmit }) => {
  const { createProject, updateProject } = useProjectManagement();

  const handleSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const projectData = Object.fromEntries(formData);
    if (projectData.id) {
      updateProject(projectData);
    } else {
      createProject(projectData);
    }
    onSubmit && onSubmit(projectData);
  };

  return (
    <VStack as='form' onSubmit={handleSubmit} spacing={4}>
      <FormControl isRequired>
        <FormLabel htmlFor='project-name'>Project Name</FormLabel>
        <Input id='project-name' name='name' placeholder='Enter project name' />
      </FormControl>
      <FormControl>
        <FormLabel htmlFor='project-description'>Description</FormLabel>
        <Input id='project-description' name='description' placeholder='Enter project description' />
      </FormControl>
      <Button type='submit' colorScheme='blue'>Submit</Button>
    </VStack>
  );
};
