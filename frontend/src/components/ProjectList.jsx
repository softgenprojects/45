import React from 'react';
import { Box, List, ListItem, IconButton, Text } from '@chakra-ui/react';
import { EditIcon, DeleteIcon } from '@chakra-ui/icons';
import { useProjectManagement } from '@hooks/useProjectManagement';

export const ProjectList = ({ projects, onEdit, onDelete }) => {
  const { updateProject, deleteProject } = useProjectManagement();

  return (
    <Box>
      <List spacing={3}>
        {Array.isArray(projects) ? (
          projects.map((project) => (
            <ListItem key={project.id} d='flex' justifyContent='space-between' alignItems='center' p={4} boxShadow='md' borderRadius='md'>
              <Text fontSize='lg'>{project.name}</Text>
              <Box>
                <IconButton
                  icon={<EditIcon />}
                  onClick={() => {
                    onEdit(project);
                    updateProject(project);
                  }}
                  mr={2}
                  aria-label='Edit project'
                />
                <IconButton
                  icon={<DeleteIcon />}
                  onClick={() => {
                    onDelete(project.id);
                    deleteProject(project.id);
                  }}
                  aria-label='Delete project'
                />
              </Box>
            </ListItem>
          ))
        ) : (
          <Text>No projects are available or an error has occurred.</Text>
        )}
      </List>
    </Box>
  );
};