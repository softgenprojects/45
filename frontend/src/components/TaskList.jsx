import React from 'react';
import { List, ListItem, Button, Flex } from '@chakra-ui/react';
import { useTaskManagement } from '@hooks/useTaskManagement';

export const TaskList = ({ tasks, onAssign, onUpdate }) => {
  const { assignTask, updateTask } = useTaskManagement();

  return (
    <List spacing={3}>
      {tasks.map((task) => (
        <ListItem key={task.id} p={4} boxShadow='md' borderRadius='md'>
          <Flex justify='space-between' align='center'>
            <span>{task.title}</span>
            <Flex>
              <Button size='sm' colorScheme='blue' onClick={() => onAssign ? onAssign(task) : assignTask(task.id)}>
                Assign
              </Button>
              <Button ml={2} size='sm' colorScheme='green' onClick={() => onUpdate ? onUpdate(task) : updateTask(task.id)}>
                Update
              </Button>
            </Flex>
          </Flex>
        </ListItem>
      ))}
    </List>
  );
};
