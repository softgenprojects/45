import React from 'react';
import { FormControl, FormLabel, Input, Button, Textarea } from '@chakra-ui/react';
import { useTaskManagement } from '@hooks/useTaskManagement';

export const TaskForm = ({ onSubmit }) => {
  const { createTask, updateTask } = useTaskManagement();
  const handleSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const taskData = Object.fromEntries(formData.entries());
    if (taskData.id) {
      updateTask(taskData);
    } else {
      createTask(taskData);
    }
    onSubmit && onSubmit(taskData);
  };

  return (
    <FormControl as='form' onSubmit={handleSubmit}>
      <FormLabel htmlFor='title'>Title</FormLabel>
      <Input id='title' name='title' type='text' placeholder='Enter task title' isRequired />

      <FormLabel htmlFor='description'>Description</FormLabel>
      <Textarea id='description' name='description' placeholder='Enter task description' isRequired />

      <Button mt={4} colorScheme='blue' type='submit'>Submit</Button>
    </FormControl>
  );
};
