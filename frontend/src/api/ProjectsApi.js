import axios from 'axios';

export const getProjects = () => {
  return axios.get('https://45-api.app.softgen.ai/api/projects')
    .then(response => response.data)
    .catch(error => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        return { error: true, message: error.response.statusText, statusCode: error.response.status };
      } else if (error.request) {
        // The request was made but no response was received
        return { error: true, message: 'No response was received', statusCode: null };
      } else {
        // Something happened in setting up the request that triggered an Error
        return { error: true, message: error.message, statusCode: null };
      }
    });
};

export const createProject = (data) => {
  return axios.post('/api/projects', data);
};

export const updateProject = (id, data) => {
  return axios.put(`/api/projects/${id}`, data);
};

export const deleteProject = (id) => {
  return axios.delete(`/api/projects/${id}`);
};