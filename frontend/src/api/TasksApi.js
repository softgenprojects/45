import axios from 'axios';

export const createTask = (data) => {
  return axios.post('/api/tasks', data);
};

export const assignTask = (id, userId) => {
  return axios.put(`/api/tasks/${id}/assign`, { userId });
};

export const updateTask = (id, data) => {
  return axios.put(`/api/tasks/${id}`, data);
};

export const trackTaskProgress = (id) => {
  return axios.get(`/api/tasks/${id}/progress`);
};