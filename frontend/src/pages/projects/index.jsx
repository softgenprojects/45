import React from 'react';
import { Container, Heading, useToast, Spinner, Center, Alert, AlertIcon, AlertTitle, AlertDescription } from '@chakra-ui/react';
import { ProjectForm } from '@components/ProjectForm';
import { ProjectList } from '@components/ProjectList';
import { useProjectManagement } from '@hooks/useProjectManagement';

const ProjectsPage = () => {
  const { projects, isLoading, isError, createProject, updateProject, deleteProject } = useProjectManagement();
  const toast = useToast();

  const handleFormSubmit = (projectData) => {
    const action = projectData.id ? updateProject : createProject;
    action(projectData, {
      onSuccess: () => {
        toast({
          title: `Project ${projectData.id ? 'updated' : 'created'}.`,
          description: `The project has been ${projectData.id ? 'updated' : 'added'}.`,
          status: 'success',
          duration: 5000,
          isClosable: true,
        });
      },
      onError: () => {
        toast({
          title: 'Error.',
          description: 'There was an error processing your request.',
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
      }
    });
  };

  const handleEdit = (project) => {
    // No changes needed for handleEdit
  };

  const handleDelete = (projectId) => {
    deleteProject(projectId);
  };

  let content;
  if (isLoading) {
    content = (
      <Center py={10}>
        <Spinner />
      </Center>
    );
  } else if (isError) {
    content = (
      <Alert status='error'>
        <AlertIcon />
        <AlertTitle mr={2}>Error!</AlertTitle>
        <AlertDescription>There was an error loading the projects.</AlertDescription>
      </Alert>
    );
  } else {
    content = (
      <ProjectList projects={projects || []} onEdit={handleEdit} onDelete={handleDelete} />
    );
  }

  return (
    <Container maxW='container.xl' py={5}>
      <Heading mb={6}>Project Management</Heading>
      <ProjectForm onSubmit={handleFormSubmit} />
      {content}
    </Container>
  );
};

export default ProjectsPage;
