import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, SimpleGrid, Image, Icon, Stack, Link } from '@chakra-ui/react';
import { FaTasks, FaChartLine, FaUsers } from 'react-icons/fa';
import { BrandLogo } from '@components/BrandLogo';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Box bg={bgColor} color={textColor} minH="100vh">
      <Center py="12" px={{ base: '4', lg: '8' }}>
        <VStack spacing="8">
          <BrandLogo />
          <Heading as="h1" size="2xl" textAlign="center">
            Project Management Simplified
          </Heading>
          <Text textAlign="center" maxW="lg">
            Organize, track, and manage your team's work with ease. Experience the power of seamless project management.
          </Text>
          <Button colorScheme="blue" size="lg">
            Get Started
          </Button>
        </VStack>
      </Center>

      <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10} py={10} px={{ base: 4, lg: 8 }}>
        <FeatureCard
          icon={FaTasks}
          title="Task Management"
          description="Create, assign, and prioritize tasks with a simple drag-and-drop interface."
        />
        <FeatureCard
          icon={FaChartLine}
          title="Analytics & Reports"
          description="Gain insights into your projects with real-time analytics and custom reports."
        />
        <FeatureCard
          icon={FaUsers}
          title="Team Collaboration"
          description="Collaborate with your team in real-time, with built-in chat and file sharing."
        />
      </SimpleGrid>

      <Box as="section" py={10}>
        <Heading as="h2" size="xl" textAlign="center" mb={6}>
          Trusted by Teams Worldwide
        </Heading>
        <Stack direction={{ base: 'column', md: 'row' }} spacing={4} justify="center" align="center">
          <Image src="https://picsum.photos/200/100" alt="Company 1" />
          <Image src="https://picsum.photos/200/100" alt="Company 2" />
          <Image src="https://picsum.photos/200/100" alt="Company 3" />
          <Image src="https://picsum.photos/200/100" alt="Company 4" />
        </Stack>
      </Box>

      <Box as="section" bgGradient='linear(to-r, blue.200, pink.500)' color='white' py={10} textAlign="center">
        <Heading as="h2" size="xl" mb={6}>
          Ready to get started?
        </Heading>
        <Text fontSize="lg" mb={6}>
          Sign up for free and start managing your projects today.
        </Text>
        <Button colorScheme="whiteAlpha" variant="outline" size="lg">
          Sign Up Now
        </Button>
      </Box>
    </Box>
  );
};

const FeatureCard = ({ icon, title, description }) => (
  <VStack spacing={4} textAlign="center">
    <Icon as={icon} boxSize={12} />
    <Heading as="h3" size="md">
      {title}
    </Heading>
    <Text color={useColorModeValue('gray.600', 'gray.200')}>
      {description}
    </Text>
  </VStack>
);

export default Home;
