import React, { useState } from 'react';
import { Box, Heading, VStack } from '@chakra-ui/react';
import { TaskForm } from '@components/TaskForm';
import { TaskList } from '@components/TaskList';
import { useTaskManagement } from '@hooks/useTaskManagement';

const TasksPage = () => {
  const { tasks, createTask, assignTask, updateTask } = useTaskManagement();
  const [taskUpdated, setTaskUpdated] = useState(false);

  const handleTaskSubmit = (taskData) => {
    createTask(taskData);
    setTaskUpdated(true);
  };

  const handleTaskAssign = (task) => {
    assignTask(task.id);
    setTaskUpdated(true);
  };

  const handleTaskUpdate = (task) => {
    updateTask(task);
    setTaskUpdated(true);
  };

  return (
    <Box p={8}>
      <VStack spacing={8}>
        <Heading as='h1' size='xl'>Tasks Management</Heading>
        <TaskForm onSubmit={handleTaskSubmit} />
        <TaskList
          tasks={tasks}
          onAssign={handleTaskAssign}
          onUpdate={handleTaskUpdate}
        />
      </VStack>
    </Box>
  );
};

export default TasksPage;
