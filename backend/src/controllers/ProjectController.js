const expressAsyncHandler = require('express-async-handler');
const { createProject, updateProject, deleteProject } = require('@services/ProjectService');

const createProjectHandler = expressAsyncHandler(async (req, res) => {
  const project = await createProject(req.body);
  res.status(201).json(project);
});

const updateProjectHandler = expressAsyncHandler(async (req, res) => {
  const { id } = req.params;
  const project = await updateProject(parseInt(id), req.body);
  res.status(200).json(project);
});

const deleteProjectHandler = expressAsyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    await deleteProject(id);
    res.status(204).send();
  } catch (error) {
    if (error.message.includes('Cannot delete project with associated tasks.')) {
      res.status(400).json({ error: error.message });
    } else {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
});

module.exports = {
  createProjectHandler,
  updateProjectHandler,
  deleteProjectHandler
};