const expressAsyncHandler = require('express-async-handler');
const { createTask, assignTask, updateTask, trackTaskProgress } = require('@services/TaskService');

const createTaskController = expressAsyncHandler(async (req, res) => {
  const task = await createTask(req.body);
  res.status(201).json(task);
});

const assignTaskController = expressAsyncHandler(async (req, res) => {
  const { id: taskId } = req.params;
  const { userId } = req.body;
  const task = await assignTask(taskId, userId);
  res.status(200).json(task);
});

const updateTaskController = expressAsyncHandler(async (req, res) => {
  const taskId = parseInt(req.params.id);
  const taskData = { id: taskId, ...req.body };
  const task = await updateTask(taskData);
  res.status(200).json(task);
});

const trackTaskProgressController = expressAsyncHandler(async (req, res) => {
  const taskId = req.params.id;
  const task = await trackTaskProgress(taskId);
  res.status(200).json(task);
});

module.exports = {
  createTaskController,
  assignTaskController,
  updateTaskController,
  trackTaskProgressController
};