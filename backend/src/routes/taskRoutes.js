const express = require('express');
const { createTaskController, assignTaskController, updateTaskController, trackTaskProgressController } = require('@controllers/TaskController');

const router = express.Router();

router.post('/tasks', createTaskController);
router.post('/tasks/:id/assign', assignTaskController);
router.put('/tasks/:id', updateTaskController);
router.get('/tasks/:id/track', trackTaskProgressController);

module.exports = router;