const express = require('express');
const { createProjectHandler, updateProjectHandler, deleteProjectHandler } = require('@controllers/ProjectController');

const router = express.Router();

router.post('/projects', createProjectHandler);
router.put('/projects/:id', updateProjectHandler);
router.delete('/projects/:id', deleteProjectHandler);

module.exports = router;