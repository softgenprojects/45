const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function createTask(taskData) {
  return await prisma.task.create({
    data: taskData,
  });
}

async function assignTask(taskId, userId) {
  return await prisma.task.update({
    where: { id: parseInt(taskId) },
    data: { assignedTo: parseInt(userId) },
  });
}

async function updateTask(taskData) {
  const { id, ...updateData } = taskData;
  if (id === undefined) {
    throw new Error('Task ID is undefined');
  }
  return await prisma.task.update({
    where: { id: parseInt(id) },
    data: updateData,
  });
}

async function trackTaskProgress(taskId) {
  const parsedTaskId = parseInt(taskId);
  return await prisma.task.findUnique({
    where: { id: parsedTaskId },
  });
}

module.exports = {
  createTask,
  assignTask,
  updateTask,
  trackTaskProgress,
};