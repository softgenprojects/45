const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function createProject(projectData) {
  return await prisma.project.create({
    data: projectData
  });
}

async function updateProject(projectId, projectData) {
  return await prisma.project.update({
    where: { id: parseInt(projectId) },
    data: projectData
  });
}

async function deleteProject(projectId) {
  const transaction = await prisma.$transaction(async (prisma) => {
    await prisma.task.deleteMany({
      where: { projectId: parseInt(projectId) }
    });
    return await prisma.project.delete({
      where: { id: parseInt(projectId) }
    });
  });
  return transaction;
}

module.exports = { createProject, updateProject, deleteProject };
