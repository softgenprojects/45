const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function main() {
  // Seed Projects
  const project1 = await prisma.project.create({
    data: {
      name: 'Project Alpha',
      description: 'First project in the system',
    },
  });

  const project2 = await prisma.project.create({
    data: {
      name: 'Project Beta',
      description: 'Second project with multiple tasks',
    },
  });

  // Seed Tasks for Project Alpha
  await prisma.task.createMany({
    data: [
      {
        title: 'Task 1',
        description: 'Task 1 description',
        status: 'completed',
        projectId: project1.id,
      },
      {
        title: 'Task 2',
        description: 'Task 2 description',
        projectId: project1.id,
      },
    ],
  });

  // Seed Tasks for Project Beta
  await prisma.task.createMany({
    data: [
      {
        title: 'Task 3',
        description: 'Task 3 description',
        status: 'in progress',
        projectId: project2.id,
      },
      {
        title: 'Task 4',
        description: 'Task 4 description',
        status: 'pending',
        projectId: project2.id,
      },
      {
        title: 'Task 5',
        description: 'Task 5 description',
        status: 'completed',
        projectId: project2.id,
      },
    ],
  });

  console.log('Database has been seeded.');
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });