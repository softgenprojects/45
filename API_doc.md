# Backend API Documentation

This document provides the details of the backend API endpoints for the frontend-handover process. It includes all the necessary information for frontend developers to integrate with the backend services effectively.

## Base URL

All API requests are made to the base URL: `https://45-api.app.softgen.ai`

## Projects

### Create Project

- **URL**: `https://45-api.app.softgen.ai/api/projects`
- **Method**: `POST`
- **Body**: 
  ```json
  {
    "name": "string",
    "description": "string"
  }
  ```
- **Success Response**: `201 Created`
- **Error Response**: `400 Bad Request`, `500 Internal Server Error`

### Update Project

- **URL**: `https://45-api.app.softgen.ai/api/projects/:id`
- **Method**: `PUT`
- **URL Parameters**: `id=[integer]`
- **Body**: 
  ```json
  {
    "name": "string",
    "description": "string"
  }
  ```
- **Success Response**: `200 OK`
- **Error Response**: `400 Bad Request`, `404 Not Found`, `500 Internal Server Error`

### Delete Project

- **URL**: `https://45-api.app.softgen.ai/api/projects/:id`
- **Method**: `DELETE`
- **URL Parameters**: `id=[integer]`
- **Success Response**: `204 No Content`
- **Error Response**: `400 Bad Request`, `404 Not Found`, `500 Internal Server Error`

## Tasks

### Create Task

- **URL**: `https://45-api.app.softgen.ai/api/tasks`
- **Method**: `POST`
- **Body**: 
  ```json
  {
    "title": "string",
    "description": "string",
    "status": "string",
    "projectId": "integer"
  }
  ```
- **Success Response**: `201 Created`
- **Error Response**: `400 Bad Request`, `500 Internal Server Error`

### Assign Task

- **URL**: `https://45-api.app.softgen.ai/api/tasks/:id/assign`
- **Method**: `POST`
- **URL Parameters**: `id=[integer]`
- **Body**: 
  ```json
  {
    "userId": "integer"
  }
  ```
- **Success Response**: `200 OK`
- **Error Response**: `400 Bad Request`, `404 Not Found`, `500 Internal Server Error`

### Update Task

- **URL**: `https://45-api.app.softgen.ai/api/tasks/:id`
- **Method**: `PUT`
- **URL Parameters**: `id=[integer]`
- **Body**: 
  ```json
  {
    "title": "string",
    "description": "string",
    "status": "string",
    "assignedTo": "integer"
  }
  ```
- **Success Response**: `200 OK`
- **Error Response**: `400 Bad Request`, `404 Not Found`, `500 Internal Server Error`

### Track Task Progress

- **URL**: `https://45-api.app.softgen.ai/api/tasks/:id/track`
- **Method**: `GET`
- **URL Parameters**: `id=[integer]`
- **Success Response**: `200 OK`
- **Error Response**: `400 Bad Request`, `404 Not Found`, `500 Internal Server Error`

## Authentication

No authentication method is used for the current endpoints.

## Error Handling

The API uses standard HTTP response codes to indicate the success or failure of an API request. In general:

- `2xx` codes indicate success.
- `4xx` codes indicate an error that failed given the information provided (e.g., a required parameter was omitted, a project or task could not be found, etc.).
- `5xx` codes indicate an error with our servers.

## Examples

The following are example cURL requests for each endpoint:

### Create Project

```
curl -X POST https://45-api.app.softgen.ai/api/projects \
     -H 'Content-Type: application/json' \
     -d '{"name":"Project Gamma","description":"Third project in the system"}'
```

### Update Project

```
curl -X PUT https://45-api.app.softgen.ai/api/projects/1 \
     -H 'Content-Type: application/json' \
     -d '{"name":"Project Delta","description":"Updated description"}'
```

### Delete Project

```
curl -X DELETE https://45-api.app.softgen.ai/api/projects/1
```

### Create Task

```
curl -X POST https://45-api.app.softgen.ai/api/tasks \
     -H 'Content-Type: application/json' \
     -d '{"title":"Task 6","description":"Task 6 description","status":"pending","projectId":1}'
```

### Assign Task

```
curl -X POST https://45-api.app.softgen.ai/api/tasks/1/assign \
     -H 'Content-Type: application/json' \
     -d '{"userId":2}'
```

### Update Task

```
curl -X PUT https://45-api.app.softgen.ai/api/tasks/1 \
     -H 'Content-Type: application/json' \
     -d '{"title":"Task 1 Updated","description":"Updated description","status":"completed","assignedTo":3}'
```

### Track Task Progress

```
curl -X GET https://45-api.app.softgen.ai/api/tasks/1/track
```

Please note that the above cURL commands are examples and may need to be adjusted based on the actual data and IDs in your environment.

End of API Documentation.